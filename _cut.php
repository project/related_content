<?php

      // I should so be using module_invoke here -- @TODO with many small hook plan
//      $related_content_method_hook = $module .'_related_content_method';
//      $expanded = $related_content_method_hook('expand', $method, $extra);








/**
 * Run a hook function for all modules that implement it.
 *
 * Taken from http://www.24b6.net/?p=194
 *
 * @param $hook_name
 *   The name of the hook to call "their_module_mymodule_hooks"
 * @param $op
 *   The operation that each module's hook should run (default info).
 * @param $data
 *   Optional data to pass the function.
 * @param $config
 *   Optional configuration information to pass to the function.
 * @return array
 *   An array of data objects (or arrays etc) returned by the hooks.
 *
 * @TODO ben-agaric:  does static caching make any sense here?
 *
 */
function related_content_extend($hook_name, $op = 'info', $data = null, $config = null) {
  static $items = array();
  if (!isset($items[$op])) {
    $items[$op] = array();
    foreach (module_implements($hook_name) as $module) {
      if ($new = module_invoke($module, $hook_name, $op, $data, $config)) {
        // somewhat related_content specific, but it's nice to know the module
        if ($op == 'info' || $op == 'display' || $op == 'ajax') {
          foreach ($new as $key => $value) {
            $new[$key]['module'] = $module;
          }
        }
        $items[$op] = array_merge($items[$op], $new);
      }
    }
  }
  return $items[$op];
}





